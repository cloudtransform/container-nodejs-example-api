var Request = require("request");

exports.getWeather = function(req, res) {

  //For this example we make use of an external API that returns the weather in the specific location.
  var location = req.params.location
  Request.get('http://weerlive.nl/api/json-data-10min.php?key=f8a741c6f2&locatie=' + location, (error, response, body) => {
    if(error){
      return console.log(error);
    }

    var obj = JSON.parse(body);
    console.error("Temperature", obj);

    // res.status(200).send(obj).end();
        // res.writeHead(200, {"Content-Type": "text/css"});
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write('<style>body{ background-color:white; color: black;} </style>');

        res.write('<h1>Weather</h1><b>At this moment it is ' + obj.liveweer[0].temp + '&#8451; in ' + location + '</b>');
        res.write('<br><h2>Full response</h2><p>' + body + '</p>')
        res.end();
  });
};
