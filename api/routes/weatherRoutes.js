'use strict';
module.exports = function(app) {
  var weatherController = require('../controllers/weatherController');

    //You can add multiple options at one call
    app.route('/weather/:location')
    .get(weatherController.getWeather)
    // .post(weatherController.postWeather)
};