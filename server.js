var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Apply the router files to the router itself.
var routes = require('./api/routes/weatherRoutes');
routes(app); 

app.get('/', function(req,res) {
    return res.status(200).send("Hello, the API is up and running!")
})

//Bad gateway message
app.use(function(req, res) {
	console.error("404 Bad Gateway on " + req.originalUrl)
	return res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(port);

console.log('|------------------------------------|');
console.log('| NodeJS API listening on port: ' + port + " |");
console.log('|------------------------------------|');